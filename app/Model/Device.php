<?php	
	class Device extends AppModel {
		public $belongsTo = array(
	        'userDevice' => array(
	            'className' => 'User',
	            'foreignKey' => 'user_id'
	        )
    	);
    	public $hasMany = array(
	        'notifDevice' => array(
	            'className' => 'Notification'
	        )
    	);

	}
?>