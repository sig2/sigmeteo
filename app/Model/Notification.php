<?php	
	class Notification extends AppModel {
		public $belongsTo = array(
	        'notifDevice' => array(
	            'className' => 'Device',
	            'foreignKey' => 'device_id'
	        ),
	        'notifType' => array(
	            'className' => 'TypeNotification',
	            'foreignKey' => 'type_notification_id'
	        )	
    	);
	}
?>