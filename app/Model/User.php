<?php	
	App::uses('AppModel', 'Model');
	App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

	class User extends AppModel {
		public $name = 'User';
		public $validate = array(
			'username' => array(
				'required' => array(
					'rule' => array('email'),
					'message' => 'Veuillez entrer votre adresse mail.'
				),
				'maxLength' => array(
					'rule' => array('maxLength', 50),
					'message' => 'Votre adresse ne peut pas dépasser 50 caractères.'
				),
				'unique' => array(
					'rule' => 'isUnique',
					'message' => 'L adresse est déjà utilisée.'
				)
			),
			'password' => array(
				'required' => array(
					'rule' => 'notBlank',
					'message' => 'Un mot de passe est requis'
				)
			),
			'role_id' => array(
				'valid' => array(
					'rule' => array('inList', array(1, 2)),
					'message' => 'Merci de rentrer un rôle valide',
					'allowEmpty' => false
				)
			)
		);
		
		/* Hash du password avant l'enregistrement en DB */
		
		public function beforeSave($options = array()) {
			if (isset($this->data[$this->alias]['password'])) {
				$passwordHasher = new SimplePasswordHasher();
				$this->data[$this->alias]['password'] = $passwordHasher->hash(
					$this->data[$this->alias]['password']
				);
			}
			return true;
		}
		
		/* Liaison */
		
		public $hasMany = array(
	        'horaireUser' => array(
	            'className' => 'Horaire'
	        ),
	        'deviceUser' => array(
	            'className' => 'Device'
	        )
    	);
    	public $belongsTo = array(	        
	        'roleUser' => array(
	            'className' => 'Role',
	            'foreignKey' => 'role_id'
	        )	 

    	);
	}
?>