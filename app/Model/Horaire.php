<?php	
	class Horaire extends AppModel {
		public $belongsTo = array(
	        'horaireUser' => array(
	            'className' => 'User',
	            'foreignKey' => 'user_id'
	        ),
	        'horaireType' => array(
	            'className' => 'TypeHoraire',
	            'foreignKey' => 'type_horaire_id'
	        )
    	);
	}
?>