<?php

class NotificationsController extends AppController {	
	
	public function index() {    	
    	        
        $IdUser = (int)$this->Session->read('Auth.User.id');
        $query = "select *
            from notifications as Notification
            inner join devices as notifDevice on notifDevice.id = Notification.device_id
            inner join type_notifications as notifType on notifType.id = Notification.type_notification_id
            where user_id = ".$IdUser;
        
        $this->set('notifications', $this->Notification->query($query));

        //Lire une variable de session
        //$this->set('session', $this->Session->read('Auth.User.id'));
        
        
	}

	public function temp_graph($id, $nb = null){
	    if($nb == null){
            $this->set("id_device", $id."/");
        }
        else{
            $this->set("id_device", "");
        }
	    if(isset($nb)){

            $this->set('notifications', $this->Notification->find('all', array(
                'conditions' => array('device_id ' => $id),
                'limit' => $nb,
                'order' => 'Notification.id DESC',
            )));
        }
        else{
            $this->set('notifications', $this->Notification->find('all', array(
                'conditions' => array('device_id ' => $id),
                'order' => 'Notification.id DESC',
            )));
        }
    }
}

?>