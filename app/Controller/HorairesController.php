<?php

class HorairesController extends AppController {	
	
	public function index($id = null) {		
		$this->loadModel('TypeHoraire');
    	$this->set('horaires', $this->Horaire->find('all'));
    	$this->set('typehoraires', $this->TypeHoraire->find('all'));
	}

	public function add(){
        if($this->request->is('post')){
            $IdUser = (int)$this->Session->read('Auth.User.id');
            $typeHoraire = $this->request->data('horaireType');
            $depart = $this->request->data('horaireDepart');
            $arrivee = $this->request->data('horaireArrivee');

            $this->Horaire->save(
                array(
                    'user_id' => $IdUser,
                    'type_horaire_id' => $typeHoraire,
                    'depart_horaire' => $depart,
                    'arrivee_horaire' => $arrivee
                )
            );
            $this->redirect(array("controller" => "Horaires",
                "action" => "Index"));
        }
    }

	public function edit($id = null) {
	    if (!$id) {
	        throw new NotFoundException(__('Invalid post'));
	    }

	    $horaire = $this->Horaire->findById($id);
	    if (!$horaire) {
	        throw new NotFoundException(__('Invalid post'));
	    }

	    if ($this->request->is(array('post', 'put'))) {
	        $this->Horaire->id = $id;
	        if ($this->Horaire->save($this->request->data)) {
	            $this->Flash->success(__('Your post has been updated.'));
	            return $this->redirect(array('action' => 'index'));
	        }
	        $this->Flash->error(__('Unable to update your post.'));
	    }

	    if (!$this->request->data) {
	        $this->request->data = $horaire;
	    }
	}

	public function delete($id) {
	    if ($this->request->is('get')) {
	        throw new MethodNotAllowedException();
	    }
	    if ($this->Horaire->delete($id)) {
	        $this->Flash->success(
	            __('L\'horaire a été supprimé avec succès.', h($id))
	        );
	    } else {
	        $this->Flash->error(
	            __('L\'horaire n\'a pas pu être supprimé.', h($id))
	        );
	    }
	    return $this->redirect(array('action' => 'index'));
	}

}

?>