<?php

class UsersController extends AppController {	

	// Permet aux utilisateurs de s'enregistrer et de se déconnecter
	public function beforeFilter() {
		parent::beforeFilter();
		// Permet aux visiteurs de s'enregistrer et de se connecter
		$this->Auth->allow('login','add');
	}

	public function login() {
		if ($this->request->is('post')) 
		{
			if ($this->Auth->login()) 
			{
				return $this->redirect(array('controller' => 'notifications', 'action' => 'index'));
			} 
			else 
			{
				$this->Flash->error(__("Nom d'utilisateur ou mot de passe invalide, Veuillez réessayer"));
			}
		}
	}

	public function logout() {
		return $this->redirect($this->Auth->logout());
	}

    public function index() {
        return $this->redirect(array('action' => 'view'));
    }

    public function view() {
		$id = $this->Session->read('Auth.User.id');
		$this->User->id = $id;
		if(!$this->User->exists())
		{
            return $this->redirect(array('action' => 'login'));
        }
        $this->set('user', $this->User->findById($id));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
			$this->request->data['User']['role_id'] = 2;
            if ($this->User->save($this->request->data))
			{
                $this->Flash->success(__('L\'user a été sauvegardé'));
                return $this->redirect(array('action' => 'index'));
            } 
			else 
			{
                $this->Flash->error(__('L\'user n\'a pas été sauvegardé. Merci de réessayer.'));
            }
        }
    }

    public function edit() {
        $id = $this->Session->read('Auth.User.id');
        $this->User->id = $id;
		
        if (!$this->User->exists()) 
		{
            return $this->redirect(array('action' => 'login'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Les modifications ont été sauvegardées'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('Les modifications n\'ont pas été sauvegardées. Merci de réessayer.'));
            }
        } else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete() {
		$id = $this->Session->read('Auth.User.id');
        $this->request->allowMethod('post');
        $this->User->id = $id;
		
        if (!$this->User->exists()) {
            return $this->redirect(array('action' => 'login'));
        }
        if ($this->User->delete()) {
            $this->Flash->success(__('User supprimé'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('L\'user n\'a pas été supprimé'));
        return $this->redirect(array('action' => 'index'));
    }
}

?>