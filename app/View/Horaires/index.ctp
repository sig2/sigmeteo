<h1>Liste des horaires</h1>

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<?php     
    echo $this->Html->script('moment.min');
    echo $this->Html->script('daterangepicker');
    echo $this->Html->css('daterangepicker');     
?>

<div class="container">
    <?php 
        $options = array(
            'label' => 'Ajouter un horaire',
            'class' => 'btn btn-primary');
        echo $this->Form->create('Horaire', array(
            'url' => 'add',
            'type' => 'post'));   
    ?>
        <select id="horaireType" name="horaireType">
            <?php foreach ($typehoraires as $typehoraire): ?>
                <option value="<?php echo $typehoraire['TypeHoraire']['id']; ?>"><?php echo $typehoraire['TypeHoraire']['type_horaire']; ?></option>
            <?php endforeach; ?>
        </select>
        <input type="hidden" id="horaireDepart" name="horaireDepart" value="" />
        <input type="hidden" id="horaireArrivee" name="horaireArrivee" value="" /> 
        <input style="width:150px;" type="text" id='daterange' name="daterange" value="" onchange="date()" />
    <?= $this->Form->end($options);?>

    <table id="horaires" class="table table-striped table-bordered table-hover dataTable no-footer display">
        <thead>
            <tr>
                <th>Début</th>
                <th>Fin</th>
                <th>Type</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($horaires as $horaire): ?>        
            <tr>
                 <td>
                    <?php echo $horaire['Horaire']['depart_horaire']; ?>
                </td>
                <td>
                    <?php echo $horaire['Horaire']['arrivee_horaire']; ?>
                </td>
                <td>
                    <?php echo $horaire['horaireType']['type_horaire']; ?>
                </td>
                <td>
                    <?php echo $this->Form->postLink(
                        'Supprimer',
                        array('action' => 'delete', $horaire['Horaire']['id']),
                        array( 'class' => 'btn btn-warning'),
                        array('confirm' => 'Etes-vous sûr ?')
                        );
                    ?>
                </td>
            </tr>        
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready( function () {
        $('#horaires').DataTable();
    } );
    //initialisation du datarangepicker
    $('input[name="daterange"]').daterangepicker({
        timePicker: true,
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(32, 'hour'),
        locale: {
          format: 'YYYY-MM-DD hh:mm:ss A'
        }
    });    
    console.log(document.getElementById('type_horaire_id'));
    function date(){
        
        var depart = document.getElementById('daterange').value.substring(0, 22);
        var arrivee = document.getElementById('daterange').value.substring(25);

        //console.log(moment(depart, 'YYYY-MM-DD hh:mm:ss A').format('YYYY-MM-DD hh:mm:ss'));
        document.getElementById('horaireDepart').value = depart.substring(0,11) + get24hTime(depart.substring(11));
        console.log(depart.substring(0,11) + get24hTime(depart.substring(11)));
        
        //console.log(get24hTime(arrivee.substring(11)));
        document.getElementById('horaireArrivee').value = arrivee.substring(0,11) + get24hTime(arrivee.substring(11));
        console.log(arrivee.substring(0,11) + get24hTime(arrivee.substring(11)));

        console.log(document.getElementById('depart_horaire').value);
    }  
    function get24hTime(str){
        str = String(str).toLowerCase().replace(/\s/g, '');
        var has_am = str.indexOf('am') >= 0;
        var has_pm = str.indexOf('pm') >= 0;
        str = str.replace('am', '').replace('pm', '');
        if (str.indexOf(':') < 0) str = str + ':00';
        if (has_am) str += ' am';
        if (has_pm) str += ' pm';
        var d = new Date("1/1/2011 " + str);
        var doubleDigits = function(n){
            return (parseInt(n) < 10) ? "0" + n : String(n);
        };
        return doubleDigits(d.getHours()) + ':' + doubleDigits(d.getMinutes()) + ':' + doubleDigits(d.getSeconds());
    }
    
</script>