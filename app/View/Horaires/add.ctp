<h1>Ajouter un horaire</h1>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<?php
/*
	echo $this->Form->create('horaire');
	echo $this->Form->input('depart');
	echo $this->Form->input('arrivee');
	echo $this->Form->input('type');
	echo $this->Form->input('date');
	echo $this->Form->end('Sauvegarder l\'horaire');*/
    echo $this->Html->script('moment.min');
    echo $this->Html->script('daterangepicker');
    echo $this->Html->css('daterangepicker');     

    $options = array(
        'label' => 'Ajouter un horaire',
        'class' => 'btn btn-primary');
    echo $this->Form->create('Horaire', array(
        'type' => 'post'));   
?>   
    <input type="hidden" id="horaireUserId" name="data[horaire][user_id]" value="<?= (int)$this->Session->read('Auth.User.id'); ?>" />
    <select id="horaireType" name="data[horaire][type]">
        <?php foreach ($typehoraires as $typehoraire): ?>
            <option value="<?php echo $typehoraire['TypeHoraire']['id']; ?>"><?php echo $typehoraire['TypeHoraire']['type_horaire']; ?></option>
        <?php endforeach; ?>
    </select>
    <input type="hidden" id="horaireDepart" name="data[horaire][depart]" value="" />
    <input type="hidden" id="horaireArrivee" name="data[horaire][arrivee]" value="" /> 
    <input style="width:150px;" type="text" id='daterange' name="daterange" value="" onchange="date()" />
<?= $this->Form->end($options);?>


<script>
    //initialisation du datarangepicker
    $('input[name="daterange"]').daterangepicker({
        timePicker: true,
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(32, 'hour'),
        locale: {
          format: 'YYYY-MM-DD hh:mm:ss A'
        }
    });
    function date(){
        
        var depart = document.getElementById('daterange').value.substring(0, 22);
        var arrivee = document.getElementById('daterange').value.substring(25);

        //console.log(moment(depart, 'YYYY-MM-DD hh:mm:ss A').format('YYYY-MM-DD hh:mm:ss'));
        document.getElementById('horaireDepart').value = depart.substring(0,11) + get24hTime(depart.substring(11));
        console.log(depart.substring(0,11) + get24hTime(depart.substring(11)));
        
        //console.log(get24hTime(arrivee.substring(11)));
        document.getElementById('horaireArrivee').value = arrivee.substring(0,11) + get24hTime(arrivee.substring(11));
        console.log(arrivee.substring(0,11) + get24hTime(arrivee.substring(11)));

        console.log(document.getElementById('depart_horaire').value);

    }  
    function get24hTime(str){
        str = String(str).toLowerCase().replace(/\s/g, '');
        var has_am = str.indexOf('am') >= 0;
        var has_pm = str.indexOf('pm') >= 0;
        str = str.replace('am', '').replace('pm', '');
        if (str.indexOf(':') < 0) str = str + ':00';
        if (has_am) str += ' am';
        if (has_pm) str += ' pm';
        var d = new Date("1/1/2011 " + str);
        var doubleDigits = function(n){
            return (parseInt(n) < 10) ? "0" + n : String(n);
        };
        return doubleDigits(d.getHours()) + ':' + doubleDigits(d.getMinutes()) + ':' + doubleDigits(d.getSeconds());
    }
</script>