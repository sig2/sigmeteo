<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $this->fetch('title'); ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <?php
	$cakeVersion = __d('cake_dev', '%s', Configure::version());
	
	/* Inclusion de tous les fichiers nécessaires au template */
	
	echo $this->Html->charset();
	echo $this->Html->meta('icon');

		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('font-awesome.min');
		echo $this->Html->css('ionicons.min');
		echo $this->Html->css('AdminLTE.min');
		echo $this->Html->css('_all-skins.min'); 
    echo $this->Html->css('dataTables.bootstrap.min');    

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	
	/* Recuperation des informations d'authentification */
	$this->Flash->render('auth');
	
  ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS sidebar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
<body class="hold-transition skin-blue sidebar-collapse sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SIG-S</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SIG-Security</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
			<?php if(isset($_SESSION['Auth']['User'])) {  ?>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<span class="hidden-xs"><?php echo $_SESSION['Auth']['User']['username']; ?></span>
			</a>
            <ul class="dropdown-menu">
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <?php echo $this->Html->link('Profile','/users/view', array('class' => 'button'));  ?>
                </div>
                <div class="pull-right">
                  <?php echo $this->Html->link('Deconnexion','/users/logout', array('class' => 'button'));  ?>
                </div>
              </li>
            </ul>
			<?php } else {  
				echo $this->Html->link('Connexion','/users/login', array('class' => 'button'));
			} ?>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Navigation Principal</li>
        <li>
			<a href="<?php echo $this->Html->url(array("controller" => "notifications", "action" => "index")); ?>">
				<i class='fa fa-bell-o'></i> <span>Notifications</span>
			</a>
        </li>
        <li>
          <a href="<?php echo $this->Html->url(array("controller" => "notifications", "action" => "temp_graph", 1)); ?>">
            <i class="fa fa-pie-chart"></i>
            <span>Graphiques</span>
          </a>
        </li>
        <li>
          <a href="<?php echo $this->Html->url(array("controller" => "Horaires", "action" => "index")); ?>">
            <i class="fa fa-calendar"></i> <span>Horaires</span>
          </a>
        </li>
        <li>
          <a href="<?php echo $this->Html->url(array("controller" => "users", "action" => "view"));?>">
            <i class="fa fa-address-card"></i> <span>Profil</span>
          </a>
        </li>
		<li>
          <a href="<?php echo $this->Html->url(array("controller" => "Devices", "action" => "DeviceUser")); ?>">
            <i class="fa fa-wifi"></i> <span>Arduino</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
		<?php
		// Fetch du contenu correspondant à la route
		echo $this->fetch('content');
		// Debug
		// echo $this->element('sql_dump');
		// var_dump($_SESSION['Auth']['User']); 
		?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>CakePHP Version</b> <?php echo $cakeVersion; ?> <b>/ Template Version</b> 2.4.0
    </div>
	<!-- Theme sur : https://adminlte.io -->
    <strong>Copyright &copy; 2018 SIG-Security.</strong> All rights reserved.
  </footer>
</div>

<!-- ./wrapper -->
	<?php
		echo $this->Html->script('jquery.min');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('jquery.slimscroll.min');
		echo $this->Html->script('fastclick');
		echo $this->Html->script('adminlte.min');
		echo $this->Html->script('Chart.min');
		echo $this->Html->script('jquery.dataTables.min');    
		echo $this->Html->script('main');
	?>
</body>
</html>
