
<h1>Liste de votre equipement</h1>
<?= $this->Html->link("Ajouter un équipement", array('controller' => 'Devices', 'action' => 'Add'), array( 'class' => 'btn btn-primary'));?>
<div class="container">
    <table id="example" class="table table-striped table-bordered table-hover dataTable no-footer display">
        <thead>

        </thead>
        <tbody>
        <?php $i = 1; ?>
        <?php echo "<tr>";?>
        <?php foreach ($DeviceUser as $notification):

            ?>
                <td align="center">
                    <?php
                    echo $this->Html->link(
                    $this->Html->image("https://image.noelshack.com/fichiers/2018/25/3/1529499237-seeeduino.jpg", ["alt" => "Brownies"]),
                    "../Notifications/temp_graph/".$notification['devices']['id']."/7",
                    ['escape' => false]
                    );
                    ?>
                    <br><br>
                    <?= "<b>".$notification['devices']['no_device']."</b>" ?>
                </td>
                <?php
                if($i % 3 == 0){
                    echo "</tr><tr>";
                }
                ?>

            <?php $i++ ?>
        <?php endforeach; ?>
        </tbody>

    </table>
</div>
