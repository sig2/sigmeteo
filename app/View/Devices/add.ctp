<h2>Ajouter un Equipement</h2>
<form method="post" action="">
   <p>
       <label for="Nom">Le nom de l'appareil :</label>
       <input type="text" name="Nom" id="Nom" />

       <br />
       <label for="Ville">L'emplacement de l'appareil :</label>
       <input type="text" name="Ville" id="Ville" />

       <br />
       <label for="Longitude">La longitude :</label>
       <input type="text" name="Longitude" id="Longitude" />

       <br />
       <label for="Latitude">La latitude :</label>
       <input type="text" name="Latitude" id="Latitude" />
        <br>
       <input type="submit" value="Enregistrer" />
   </p>
</form>
<?= $this->Html->link("Retour", array('controller' => 'Devices', 'action' => 'DeviceUser'), array( 'class' => 'btn btn-primary'));?>