<style>
    .chart-container {
        position: relative;
        margin: auto;
        height: 40vh;
        width: 40vw;
    }
</style>



<?php
$notif = end($notifications);
//debug($notif);
?>

<h1>Graphique des notifications</h1>

<div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        Durée d'affichage des données
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <li><a href="<?php echo $id_device.'2'?>" title="Lien 1">2 jours</a></li>
        <li><a href="<?php echo $id_device.'5'?>" title="Lien 2">5 jours</a></li>
        <li><a href="<?php echo $id_device.'7'?>" title="Lien 3">1 semaine</a></li>
        <li><a href="<?php echo $id_device.'14'?>" title="Lien 3">2 semaines</a></li>
        <li><a href="<?php echo $id_device.'21'?>" title="Lien 3">3 semaines</a></li>
        <li><a href="<?php echo $id_device.'30'?>" title="Lien 3">1 mois</a></li>
    </ul>
</div>
<br><br><br>
<div class="container">
    <table width="100%">
        <tr >
            <td align="center">
                <div class="chart-container">
                    <canvas id="TempChart"></canvas>
                </div>
            </td>

        </tr>
        <tr style="height: 50px"></tr>
        <tr>
            <td align="center">
                <div class="chart-container">
                    <canvas id="HumidityChart"></canvas>
                </div>
            </td>
        </tr>
        <tr style="height: 50px"></tr>
    </table>


    <table id="example" class="table table-striped table-bordered table-hover dataTable no-footer">
        <tr>
            <th>Date</th>
            <th>Type</th>
            <th>Ville</th>
            <th>Température</th>
            <th>Humidité</th>
            <th>Ouverture</th>
            <th>Lumière</th>
        </tr>
        <?php
            $temperature = array();
            $humidity = array();
            $day = array();
        ?>
        <?php foreach ($notifications as $notification): ?>
            <?php echo "<div onload='tab(".$notification['Notification']['temperature_notif'] . ")'/>" ?>
            <tr>
                <td>
                    <?= $notification['Notification']['date_notif']; ?>
                    <?php
                    $pieces = explode(" ", $notification['Notification']['date_notif']);
                    ?>
                    <?php array_push($day, $pieces[0]); ?>
                </td>
                <td>
                    <?= $notification['notifType']['type_notif']; ?>
                </td>
                <td>
                    <?= $notification['notifDevice']['ville_device']; ?>
                </td>
                <td>
                    <?= $notification['Notification']['temperature_notif']; ?>
                    <?php array_push($temperature, $notification['Notification']['temperature_notif']); ?>
                </td>
                <td>
                    <?= $notification['Notification']['humidite_notif']; ?>
                    <?php array_push($humidity, $notification['Notification']['humidite_notif']); ?>
                </td>
                <td>
                    <?= $notification['Notification']['ouverture_notif']; ?>
                </td>
                <td>
                    <?= $notification['Notification']['lumiere_notif']; ?>
                </td>
            </tr>
        <?php endforeach;?>

    </table>
</div>
<script>


    var ctx = document.getElementById('TempChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: <?php echo json_encode(array_reverse($day)); ?>,
            datasets: [{
                label: "Temperature",
                backgroundColor: 'rgb(0, 0, 0,0)',
                borderColor: 'rgb(255, 99, 132)',
                data: <?php echo json_encode(array_reverse($temperature)); ?>,
            }]
        },
        options : {}
    });

    var ctx = document.getElementById('HumidityChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        data: {
            labels: <?php echo json_encode(array_reverse($day)); ?>,
            datasets: [{
                label: "Humidité",
                backgroundColor: 'rgb(0, 0, 0,0)',
                borderColor: 'rgb(148, 182, 34)',
                data: <?php echo json_encode(array_reverse($humidity)); ?>,
            }]
        }
    });
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIYgNnQkZKHKWjuG55rxd-1HG9su4YyXc&callback=initMap">
</script>