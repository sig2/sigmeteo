<style>
   #map {
    height: 400px;
    width: 60%;
   }
</style>

<h1>Localisation</h1>
<center><div id="map"></div></center>
<br/>
<?php
    $notif = end($notifications);
    //debug($session);
?>
<input type="hidden" id="lat" value="<?= $notif['notifDevice']['latitude_device'] ?>" />
<input type="hidden" id="lng" value="<?= $notif['notifDevice']['longitude_device'] ?>" />

<h1>Historique des notifications</h1>
<div class="container">
<table id="example" class="table table-striped table-bordered table-hover dataTable no-footer display">
    <thead>
        <tr>
            <th>Date</th>
            <th>Type</th>
            <th>Ville</th>
            <th>Température</th>
            <th>Humidité</th>
            <th>Ouverture</th>
            <th>Lumière</th>  
        </tr>
    </thead>
    <tbody>
        <?php foreach ($notifications as $notification): ?>
        <tr>
            <td>
                <?= $notification['Notification']['date_notif']; ?>
            </td>
            <td>
                <?= $notification['notifType']['type_notif']; ?>
            </td>
            <td>
                <?= $notification['notifDevice']['ville_device']; ?>
            </td>
            <td>
                <?= $notification['Notification']['temperature_notif']; ?>
            </td>
            <td>
                <?= $notification['Notification']['humidite_notif']; ?>
            </td>
            <td>
                <?php if($notification['Notification']['ouverture_notif']){echo ('✓');}; ?>                        
            </td>
            <td>
                <?php if($notification['Notification']['lumiere_notif']){echo ('✓');}; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>

</table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script>
  function initMap() {
    var latitude = parseFloat(document.getElementById("lat").value);
    var longitude = parseFloat(document.getElementById("lng").value);
    console.log(latitude + ' ' + longitude);
    var device = {lat: latitude, lng: longitude};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      center: device
    });
    var marker = new google.maps.Marker({
      position: device,
      map: map
    });     
  }
  $(document).ready( function () {
        $('#example').DataTable();
    } );  
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIYgNnQkZKHKWjuG55rxd-1HG9su4YyXc&callback=initMap">
</script>