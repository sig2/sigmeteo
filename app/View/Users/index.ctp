<h1>Utilisateurs</h1>
<div class="container">
<table id="exemple" class="table table-striped table-bordered table-hover dataTable no-footer">
    <tr>
        <th>Adresse mail</th>
        <th>Action</th>
    </tr>

    <?php //debug($users); ?>

    <?php foreach ($users as $user): ?>
    <tr>
         <td>
            <?php echo $user['User']['username']; ?>
        </td>
        <td>
            <?php echo $this->Html->link(
                    'Modifier',
                    array('controller' => 'users', 'action' => 'edit', $user['User']['id'])
                ); ?> | 
            <?php echo $this->Html->link(
                    'Configurer',
                    array('controller' => 'horaires', 'action' => 'index', $user['User']['id'])
                ); ?> | 
                <?php echo $this->Html->link(
                    'Notifications',
                    array('controller' => 'notifications', 'action' => 'index', $user['User']['id'])
                ); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>