<div class="users form">
<h1>Connexion au site</h1>
<p><?php echo $this->Flash->render(); ?></p> 
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <?php 
			echo $this->Form->input('username');
			echo $this->Form->input('password', array('type'=>'password'));
		?>
    </fieldset>
<?php echo $this->Form->end(__('Connexion'))." ".$this->Html->link('Inscription','/users/add', array('class' => 'button')); ?>
</div>